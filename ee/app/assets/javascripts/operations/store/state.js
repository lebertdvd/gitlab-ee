export default () => ({
  inputValue: '',
  isLoadingProjects: false,
  projectEndpoints: {
    list: null,
    add: null,
  },
  projects: [],
  projectTokens: [],
  projectSearchResults: [],
  searchCount: 0,
});
